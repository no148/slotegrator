<?php

namespace Tests\Unit;

use App\Models\Prize;
use App\Models\PrizeUser;
use App\Models\User;
use App\Services\PrizeService;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    private $moneyPrize;

    public function setUp() :void
    {
        parent::setUp();

        Artisan::call('migrate:fresh');
        $user = factory(User::class)->make();
        $user->save();

        $prizeType = (new Prize([
            'type' => Prize::PRIZE_TYPE_MONEY,
            'amount' => 500,
            'is_active' => 1,
        ]));
        $prizeType->save();

        $moneyPrize = (new PrizeUser([
            'prize_id' => $prizeType->id,
            'user_id' => $user->id,
            'amount' => rand(1, 50),
            'status' => PrizeUser::PRIZE_USER_STATUS_PENDING
        ]));
        $moneyPrize->save();
        $this->moneyPrize = $moneyPrize;
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testConvertMoneyToPoints()
    {
        $prizeService = resolve(PrizeService::class);
        $moneyPrize = $this->moneyPrize;
        $prizeService->changeMoneyToPoints($moneyPrize);

        $this->assertTrue($moneyPrize->status === PrizeUser::PRIZE_USER_STATUS_PAID);
        $this->assertEquals($moneyPrize->user->points_balance, $moneyPrize->amount * Prize::POINTS_TO_MONEY_MULTIPLIER);
    }
}
