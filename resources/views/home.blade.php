@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">MY POINTS BALANCE: <b>{{ number_format($user->points_balance) }}</b></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (isset($errors) && $errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif

                    <form action="{{ route('request-prize') }}" method="POST">
                        <button type="submit" class="btn btn-primary">GET A PRIZE!</button>
                        {{ csrf_field() }}
                    </form>

                    <h4 class="mt-lg-4">My prizes:</h4>
                    <div class="mt-lg-4">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Type</th>
                                <th scope="col">Amount/Item</th>
                                <th scope="col">Status</th>
                                <th scope="col">Received</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($user->prizes as $prize)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{ $prize->prize->getStringStatus() }}</td>
                                        @if($prize->prize->type === \App\Models\Prize::PRIZE_TYPE_ITEM)
                                            <td>{{ $prize->item }}</td>
                                        @else
                                            <td>{{ number_format($prize->amount, 2) }}</td>
                                        @endif

                                        <td>{{ $prize->getStringStatus() }}</td>
                                        <td>{{ \Carbon\Carbon::parse($prize->created_at, 'Y-m-d H:i:s') }}</td>
                                        <td>
                                            @if($prize->status === \App\Models\PrizeUser::PRIZE_USER_STATUS_PENDING && $prize->prize->type === \App\Models\Prize::PRIZE_TYPE_MONEY)
                                            <a href="{{ route('money-to-points', ['prizeId' => $prize->id]) }}" class="btn btn-sm btn-primary" role="button">change</a>
                                            @endif
                                            @if($prize->status === \App\Models\PrizeUser::PRIZE_USER_STATUS_PENDING || $prize->prize->type === \App\Models\Prize::PRIZE_TYPE_POINTS)
                                            <a href="{{ route('reject-prize', ['prizeId' => $prize->id]) }}" class="btn btn-sm btn-danger" role="button">reject</a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">No prizes yet</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
