<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group([
    'web',
], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/request-prize', 'HomeController@requestPrize')->name('request-prize');
    Route::get('/reject-prize/{prizeId}', 'HomeController@rejectPrize')->name('reject-prize');
    Route::get('/money-to-points/{prizeId}', 'HomeController@changeMoneyToPoints')->name('money-to-points');
});
