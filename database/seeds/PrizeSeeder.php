<?php

use App\Models\Prize;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prizes')->insert([
            'type' => Prize::PRIZE_TYPE_MONEY,
            'amount' => 5000,
        ]);

        DB::table('prizes')->insert([
            'type' => Prize::PRIZE_TYPE_POINTS,
        ]);

        DB::table('prizes')->insert([
            'type' => Prize::PRIZE_TYPE_ITEM,
            'amount' => 100,
        ]);
    }
}
