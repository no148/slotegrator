<?php

namespace App\Providers;

use App\Services\PrizeService;
use Illuminate\Support\ServiceProvider;

class PrizeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('\App\Services\PrizeService', function() {
            return new PrizeService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
