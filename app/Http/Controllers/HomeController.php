<?php

namespace App\Http\Controllers;

use App\Services\PrizeService;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $user = Auth::user();
        return view('home')->with([
            'user' => $user
        ]);
    }

    /**
     * @param Request $request
     * @param PrizeService $prizeService
     * @return RedirectResponse
     */
    public function requestPrize(Request $request, PrizeService $prizeService) {
        $user = Auth::user();
        try {
            $prizeService->requestPrize($user);
            return redirect('home');
        } catch (\Exception $exception) {
            return redirect('home')->withErrors([$exception->getMessage()]);
        }
    }

    /**
     * @param int $prizeId
     * @param PrizeService $prizeService
     * @return RedirectResponse
     */
    public function rejectPrize(int $prizeId, PrizeService $prizeService) {
        $user = Auth::user();
        try {
            $prize = $prizeService->checkPrizeBelongsToUserById($prizeId, $user->id);
            if (!$prize){
                throw new \Exception('Error in rejecting the prize!');
            }
            $prizeService->rejectPrize($prize);

            return redirect('home');
        } catch (\Exception $exception) {
            return redirect('home')->withErrors([$exception->getMessage()]);
        }
    }

    /**
     * @param int $prizeId
     * @param PrizeService $prizeService
     * @return RedirectResponse
     */
    public function changeMoneyToPoints(int $prizeId, PrizeService $prizeService) {
        $user = Auth::user();
        try {
            $prize = $prizeService->checkPrizeBelongsToUserById($prizeId, $user->id);
            if (!$prize){
                throw new \Exception('Error in changing the money!');
            }
            $prizeService->changeMoneyToPoints($prize);

            return redirect('home');
        } catch (\Exception $exception) {
            return redirect('home')->withErrors([$exception->getMessage()]);
        }
    }
}
