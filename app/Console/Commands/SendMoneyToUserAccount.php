<?php

namespace App\Console\Commands;

use App\Models\Prize;
use App\Models\PrizeUser;
use App\Services\Repositories\PrizeRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SendMoneyToUserAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'money-prize:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends pending money prizes to user\'s account';

    const LIMIT = 2;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param PrizeRepository $prizeRepository
     * @return mixed
     */
    public function handle(PrizeRepository $prizeRepository)
    {
        $this->info("Script is started");
        $count = $this->getPrizes()->count();

        $this->info("$count - prizes to send");
        while ($count > 0) {
            $prizes = $this->getPrizes( self::LIMIT)->get();
            foreach ($prizes as $prize) {
                $this->info("Sending prize id: $prize->id / Amount: $prize->amount");
                try {
                    $this->sendMoneyToUserAccount($prize);
                    $prizeRepository->changePrizeStatus($prize, PrizeUser::PRIZE_USER_STATUS_PAID);
                    $this->info("Prize id $prize->id is sent");
                }catch (\Exception $exception) {
                    //@todo logging an error into file
                    $this->info("Prize id $prize->id sending error");
                }
            }
            $count -= self::LIMIT;
        }
        $this->info("Script is completed");
    }

    /**
     * @todo Must be moved to repository
     *
     * @param null $limit
     * @return Builder
     */
    private function getPrizes($limit = null) {
        $query = PrizeUser::with(['prize'])->whereHas('prize', function($q) {
            $q->where('type', Prize::PRIZE_TYPE_MONEY);
        })->where([
            'status' => PrizeUser::PRIZE_USER_STATUS_PENDING
        ]);

        if ($limit) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * @param $prize
     * @throws \Exception
     */
    private function sendMoneyToUserAccount($prize) :void {
        //@todo replace api call simulation
        sleep(2);
        if (rand(0,100) > 94) {
            throw new \Exception('random api error');
        }
        $bankCardNumber = $prize->user->bank_card_number;
    }
}
