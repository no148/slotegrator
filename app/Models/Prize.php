<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Prize
 * @property int $id
 * @property int $type
 * @property int $amount
 * @property int $is_active
 * @property string $created_at
 * @property string $updated_at
 * @package App
 */
class Prize extends Model
{
    const PRIZE_TYPE_MONEY = 1;
    const PRIZE_TYPE_POINTS = 2;
    const PRIZE_TYPE_ITEM = 3;
    const POINTS_TO_MONEY_MULTIPLIER = 0.5;

    public static $types = [
        self::PRIZE_TYPE_MONEY => 'money',
        self::PRIZE_TYPE_POINTS => 'points',
        self::PRIZE_TYPE_ITEM => 'item',
    ];

    //@todo it's better to have a separate table for any number of items and quantities
    const PRIZE_TYPE_ITEM_OPTIONS = [
        'a mug',
        'a pen',
        'a badge',
    ];

    /**
     * @var string
     */
    protected $table = 'prizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'amount',
        'is_active'
    ];

    /**
     * @return HasMany
     */
    public function prizeUsers() {
        return $this->hasMany('App\Models\PrizeUser', 'prize_id', 'id');
    }

    /**
     * @return string
     */
    public function getStringStatus() {
        if (isset(self::$types[$this->type])) {
            return self::$types[$this->type];
        }
        return 'Unknown';
    }
}
