<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PrizeUser extends Model
{
    const PRIZE_USER_STATUS_REJECTED = -1;
    const PRIZE_USER_STATUS_PENDING = 0;
    const PRIZE_USER_STATUS_PAID = 1;
    const PRIZE_USER_STATUS_CONVERTED = 2;
    const PRIZE_USER_STATUS_BEING_SHIPPED = 3;

    public static $statuses = [
        self::PRIZE_USER_STATUS_REJECTED => 'rejected by user',
        self::PRIZE_USER_STATUS_PENDING => 'pending',
        self::PRIZE_USER_STATUS_PAID => 'paid',
        self::PRIZE_USER_STATUS_CONVERTED => 'converted',
        self::PRIZE_USER_STATUS_BEING_SHIPPED => 'being delivered',
    ];

    /**
     * @var string
     */
    protected $table = 'user_prizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prize_id',
        'user_id',
        'item',
        'amount',
        'status',
    ];


    /**
     * @return BelongsTo
     */
    public function prize() {
        return $this->belongsTo('App\Models\Prize', 'prize_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * @return string
     */
    public function getStringStatus() {
        if (isset(self::$statuses[$this->status])) {
            return self::$statuses[$this->status];
        }

        return 'Unknown';
    }
}
