<?php

namespace App\Services\Repositories;

use App\Models\Prize;
use App\Models\PrizeUser;

/**
 * Class UserRepository
 * @package App\Services\Repositories
 */
class PrizeRepository {

    /**
     * @return mixed
     */
    public function getAvailablePrizes() {
        return Prize::where('is_active', '=',  1)
            ->where(function($query){
                $query->where('amount', '>', 0)
                    ->orWhere('amount', '=', NULL);
                })
            ->get();
    }

    /**
     * @param $prize_id
     */
    public function decrementPrizeAmountByPrizeId($prize_id) :void {
        Prize::where('id', $prize_id)->decrement('amount');
    }

    /**
     * @param $user
     * @param $newPrizeType
     * @return PrizeUser|null
     */
    public function generatePrizeByType($user, $newPrizeType) {
        $prizeUser = new PrizeUser();
        $prizeUser->prize_id = $newPrizeType->id;
        $prizeUser->user_id = $user->id;

        switch ($newPrizeType->type) {
            case Prize::PRIZE_TYPE_MONEY:
                $prizeUser->status = PrizeUser::PRIZE_USER_STATUS_PENDING;
                $prizeUser->amount = $this->generateMoneyPrize($newPrizeType);
                break;
            case Prize::PRIZE_TYPE_POINTS:
                $prizeUser->status = PrizeUser::PRIZE_USER_STATUS_PAID;
                $newPointsPrize = rand(10,500);
                $this->sendPointsToUser($user, $newPointsPrize);
                $prizeUser->amount = $newPointsPrize;
                break;
            case Prize::PRIZE_TYPE_ITEM:
                $prizeUser->status = PrizeUser::PRIZE_USER_STATUS_PENDING;
                $prizeUser->item = Prize::PRIZE_TYPE_ITEM_OPTIONS[array_rand(Prize::PRIZE_TYPE_ITEM_OPTIONS, 1)];
                $this->decrementPrizeAmountByPrizeId($newPrizeType->id);
                break;
        }

        $prizeUser->save();
        return $prizeUser->fresh();
    }

    /**
     * @param $newPrizeType
     * @return int
     */
    public function generateMoneyPrize($newPrizeType) {
        $genAmount = rand(1,50);
        $diff = $newPrizeType->amount - $genAmount;

        if ($diff >= 0) {
            $newAmount = $diff;
            $prizeAmount = $genAmount;
        }else{
            $newAmount = 0;
            $prizeAmount = $genAmount + $diff;
        }

        $newPrizeType->amount = $newAmount;
        $newPrizeType->save();

        return $prizeAmount;
    }

    /**
     * @param $user
     * @param $newPoints
     */
    public function sendPointsToUser($user, $newPoints) :void {
        $user->points_balance += $newPoints;
        $user->save();
    }

    /**
     * @param $prizeId
     * @param $userId
     * @return mixed
     */
    public function checkPrizeBelongsToUserById($prizeId, $userId) {
        return PrizeUser::where([
            'user_id' => $userId,
            'id' => $prizeId
        ])->first();
    }

    /**
     * @param $prize
     * @param $status
     */
    public function changePrizeStatus($prize, $status) :void {
        $prize->status = $status;
        $prize->save();
    }

}
