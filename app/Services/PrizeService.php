<?php

namespace App\Services;

use App\Models\Prize;
use App\Models\PrizeUser;
use App\Services\Repositories\PrizeRepository;

/**
 * Class RateService
 * @package app\Services
 */
class PrizeService {

    /**
     * @var PrizeRepository
     */
    protected $_prizeRepository;

    /**
     * PrizeService constructor.
     * @param PrizeRepository $prizeRepository
     */
    public function __construct(
        PrizeRepository $prizeRepository
    ) {
        $this->_prizeRepository = $prizeRepository;
    }

    /**
     * @param $user
     * @return PrizeUser|null
     * @throws \Exception
     */
    public function requestPrize($user) {
        $newPrizeType = $this->generatePrizeType();

        return $this->_prizeRepository->generatePrizeByType($user, $newPrizeType);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function generatePrizeType() {
        $activePrizes = $this->_prizeRepository->getAvailablePrizes();

        if ($activePrizes->isEmpty()){
            throw new \Exception('Sorry no prizes is available :`(');
        }

        return $activePrizes->random();
    }

    /**
     * @param $prizeId
     * @param $userId
     * @return mixed
     */
    public function checkPrizeBelongsToUserById($prizeId, $userId) {
        return $this->_prizeRepository->checkPrizeBelongsToUserById($prizeId, $userId);
    }

    /**
     * @param $prize
     */
    public function rejectPrize($prize) :void {
        //@todo add balance recalculation and prize amount sending back to prizes
        $this->_prizeRepository->changePrizeStatus($prize, PrizeUser::PRIZE_USER_STATUS_REJECTED);
    }

    /**
     * @param $prize
     */
    public function changeMoneyToPoints($prize) :void {
        $newPoints = $prize->amount * Prize::POINTS_TO_MONEY_MULTIPLIER;
        $this->_prizeRepository->sendPointsToUser($prize->user, $newPoints);
        $this->_prizeRepository->changePrizeStatus($prize, PrizeUser::PRIZE_USER_STATUS_PAID);
    }
}
